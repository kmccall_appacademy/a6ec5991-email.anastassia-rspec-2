#Anastassia Bobokalonova
#March 29, 2017

def measure(repeat_num=1, &block)
  total_time = 0

  repeat_num.times do
    start_time = Time.now
    block.call
    duration = Time.now - start_time
    total_time += duration
  end

  average_time = total_time / repeat_num
  average_time 
end
