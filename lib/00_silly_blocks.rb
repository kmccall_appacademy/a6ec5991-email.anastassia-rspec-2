#Anastassia Bobokalonova
#March 29, 2017

def reverser(&block)
  words = block.call
  reversed_words = []
  words.split.each {|word| reversed_words << word.reverse}
  reversed_words.join(" ")
end

def adder(num=1, &block)
  value = block.call
  value + num
end

def repeater(repeat_num=1, &block)
  repeat_num.times do
    block.call
  end
end
